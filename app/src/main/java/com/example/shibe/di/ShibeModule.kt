package com.example.shibe.di

import com.example.shibe.model.remote.ShibeService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
object ShibeModule {

    @Provides
    fun providesShibesService(): ShibeService = ShibeService.getInstance()


}