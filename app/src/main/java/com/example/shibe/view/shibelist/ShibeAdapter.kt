package com.example.shibe.view.shibelist

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.shibe.databinding.ItemShibeBinding
import com.example.shibe.model.ShibeRepo
import com.example.shibe.model.local.entity.Shibe
import com.squareup.picasso.Picasso
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ShibeAdapter(
    private val shibes: List<Shibe>,
    private val repo: ShibeRepo
) : RecyclerView.Adapter<ShibeAdapter.ShibeViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ) = ShibeViewHolder.getInstance(parent)

    override fun onBindViewHolder(holder: ShibeViewHolder, position: Int) {
        val url = shibes[position]
        holder.loadShibeImage(url, repo)
    }

    override fun getItemCount() = shibes.size

    class ShibeViewHolder(
        private val binding: ItemShibeBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun loadShibeImage(shibe: Shibe, repo: ShibeRepo) {
            with(binding) {
                Picasso.with(root.context).load(shibe.url).into(cvImage)
            }

            val color = if (shibe.isFavorite) Color.RED else Color.WHITE
            binding.imageFavHeart.setColorFilter(color)
            binding.imageFavHeart.setOnClickListener {
                shibe.isFavorite = !shibe.isFavorite
                CoroutineScope(Dispatchers.IO).launch {
                    repo.shibeDao.update(shibe)
                    val color = if (shibe.isFavorite) Color.RED else Color.WHITE
                    binding.imageFavHeart.setColorFilter(color)
                }
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemShibeBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { binding -> ShibeViewHolder(binding) }
        }
    }
}