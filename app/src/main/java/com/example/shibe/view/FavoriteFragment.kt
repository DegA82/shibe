package com.example.shibe.view

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.shibe.databinding.FragmentShibeListBinding
import com.example.shibe.model.ShibeRepo
import com.example.shibe.view.shibelist.ShibeAdapter
import com.example.shibe.viewmodel.ShibeViewModel
import dagger.hilt.EntryPoint
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FavoriteFragment : Fragment() {

    private var _binding: FragmentShibeListBinding? = null
    private val binding get() = _binding!!
    private val shibeViewModel by viewModels<ShibeViewModel>()
    @Inject lateinit var repo: ShibeRepo
//    private val repo by lazy { ShibeRepo(requireContext()) }
//    private val shibeViewModel by viewModels<ShibeViewModel> {
//        ShibeViewModel.ShibeViewModelFactory(ShibeRepo(requireContext()))
//    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeListBinding.inflate(inflater, container, false).also { _binding = it }.root

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {
            btnFav.text = "Back"
            btnFav.setOnClickListener {
                it.findNavController()
                    .navigate(FavoriteFragmentDirections.actionFavoriteFragmentToShibeListFragment())
            }
        }

        binding.btnLinear.setOnClickListener {
            val originalLayout = binding.rvShibeList.layoutManager.toString()
            if (originalLayout.contains("StaggeredGridLayoutManager")){
                binding.rvShibeList.layoutManager = LinearLayoutManager(context)
            } else if(originalLayout.contains("LinearLayoutManager")){
                binding.rvShibeList.layoutManager = GridLayoutManager(context,2)
            } else{
                binding.rvShibeList.layoutManager = StaggeredGridLayoutManager(2,
                    StaggeredGridLayoutManager.VERTICAL)
            }
        }

        shibeViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.run {
                progress.isVisible = state.isLoading
                rvShibeList.adapter = ShibeAdapter(state.shibes.filter { it.isFavorite }, repo)
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
