package com.example.shibe.view.shibelist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.example.shibe.databinding.FragmentShibeListBinding
import com.example.shibe.model.ShibeRepo
import com.example.shibe.model.local.dao.ShibeDao
import com.example.shibe.model.local.entity.Shibe
import com.example.shibe.model.remote.ShibeService
import com.example.shibe.viewmodel.ShibeViewModel
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ShibeListFragment : Fragment() {

    private var _binding: FragmentShibeListBinding? = null
    private val binding get() = _binding!!

    //    private val repo by lazy { ShibeRepo(requireContext()) }
    @Inject lateinit var repo: ShibeRepo
    private val shibeViewModel by viewModels<ShibeViewModel> {
//        ShibeViewModel.ShibeViewModelFactory(ShibeRepo(requireContext()))
        ShibeViewModel.ShibeViewModelFactory(repo)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentShibeListBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(binding) {
            btnFav.setOnClickListener {
                findNavController().navigate(ShibeListFragmentDirections.actionShibeListFragmentToFavoriteFragment())
            }
            btnLinear.setOnClickListener {
                val originalLayout = binding.rvShibeList.layoutManager.toString()
                if (originalLayout.contains("StaggeredGridLayoutManager")) {
                    binding.rvShibeList.layoutManager = LinearLayoutManager(context)
                } else if (originalLayout.contains("LinearLayoutManager")) {
                    binding.rvShibeList.layoutManager = GridLayoutManager(context, 2)
                } else {
                    binding.rvShibeList.layoutManager =
                        StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
                }
            }
//            btnGrid.setOnClickListener {
//                rvShibeList.layoutManager = GridLayoutManager(root.context, 3)
//            }
//            btnLinear.setOnClickListener {
//                rvShibeList.layoutManager = LinearLayoutManager(root.context)
//            }
//            btnStaggered.setOnClickListener {
//                rvShibeList.layoutManager = StaggeredGridLayoutManager(2, 1)
//            }
        }

        shibeViewModel.state.observe(viewLifecycleOwner) { state ->
            binding.progress.isVisible = state.isLoading
            binding.rvShibeList.apply {
                adapter = ShibeAdapter(state.shibes, repo)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}